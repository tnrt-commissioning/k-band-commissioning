from astropy.time import Time
import pandas as pd

def run_raster(ra_hms, dec_dms, xlen, xstep, ylen, ystep, time_per_point):

    skc_temp = SkyCoord(ra_hms, dec_dms)
    ra_catalog_icrs  = skc_temp.ra.deg
    dec_catalog_icrs = skc_temp.dec.deg

    pm_ra = 0
    pm_dec = 0
    parallax = 0
    radial_velocity = 0
    send_icrs_to_acu = False

    tracking_params = TrackingParamsEQ(
        ra_catalog_icrs,
        dec_catalog_icrs,
        pm_ra,
        pm_dec,
        parallax,
        radial_velocity,
        send_icrs_to_acu,
        user_pointing_correction_az,
        user_pointing_correction_el,
    )
    if activate_offsource == True:
        scantype_params = ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis,
            coord_system,
            offsource_x,
            offsource_y,
            offsource_duration,
            offsource_coord_system
        )
        
    elif activate_offsource == False:
            scantype_params = ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis,
            coord_system
        )

    backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer_K", 
        integration_time = integration_time,
        freq_res = freq_res #Hz
    )
    add_scan(
            schedule_params,
            scantype_params,
            tracking_params,
            backend_params=backend_params
        )

    results = run_queue()
    log("all results: %s" % results)

# ------------------------------------------------------------------------
# Activate the specified motor axis
# If activated already please add " # " in front of the command.
# ------------------------------------------------------------------------

#activate('m3')
#activate('hxp')
#set_constant_focus_offsets(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
# waitrel(60, True, 10)

#Set pointing model
set_pmodel(4236, 171, -19, -14, -16, 0, 2919, -643, -638)
#set_refraction(76.6, 5.7346, 2.2874)

    
# Header setting
project_id                      = "DDT_002: C/2023 A3 (Tsuchinshan-ATLAS) observation"
user_ID                         = "S.Poshyachinda / P.Chatuphot"

# Raster General setting
freq_res                        = 8000 #Hz
integration_time                = 1.0 #sec
coord_system                    = "EQ"
zigzag                          = True
primary_axis                    = "X"
user_pointing_correction_az     = -200 #arcsec
user_pointing_correction_el     = 200 #arcsec
end_time                        = "2024-10-04 03:00:00"        # Designated finish observaton time (UT)

# If want to take off source data in the raster scan.
# activate_offsource = True is use this function / False is not use this function
activate_offsource              = False  
offsource_x                     = -2600 #arcsec
offsource_y                     = 0 #arcsec
offsource_duration              = 4 #s
offsource_coord_system          = "EQ"

# Get Catalogoing 
catalog_csv                     = 'Raster_comet_20241004.csv' 
catalog_editor                  = pd.read_csv(catalog_csv)
catalog                         = get_catalog(catalog_csv)

count_numscan = 1
for i in catalog.index:
    if int(catalog['finished'][i]) == 0 and Time.now() < Time(end_time) and count_numscan < 75:
        num_scan            = int(catalog['num_scan'][i])
        ra_hms              = catalog['RA'][i]
        dec_dms             = catalog['Dec'][i]
        xlen                = int(catalog['xlen'][i])
        xstep               = int(catalog['xstep'][i])
        ylen                = int(catalog['ylen'][i])
        ystep               = int(catalog['ystep'][i])
        time_per_point      = int(catalog['time_per_point'][i])

        # Choose Schedule Parameters
        schedule_params = ScheduleParams(
            project_id = project_id, 
            obs_id = user_ID, 
            source_name = catalog['SourceName'][i],
            scan_id = num_scan, 
            line_name = "H2O maser",
        )

        log("Counting in loop: {}/74".format(count_numscan))
        log("Scaning in loop: {}/{}".format(i + 1, len(catalog['SourceName'])))
        log("Scaning at: {}".format(catalog['SourceName'][i]))
        log("Right Ascension: {}".format(ra_hms))
        log("Declination: {}".format(dec_dms))
        log('usr_pointing_corr_az: {} arcsec'.format(user_pointing_correction_az))
        log('usr_pointing_corr_el: {} arcsec'.format(user_pointing_correction_el))
        log("Length x : {} arcsec   Length y : {} arcsec".format(xlen, ylen))
        log("Step   x : {} arcsec   Step   y : {} arcsec".format(xstep, ystep))
        if activate_offsource == True:
            log("offsource activate")
        elif activate_offsource == False:
            log("offsource not activate")

        run_raster(ra_hms, dec_dms, xlen, xstep, ylen, ystep, time_per_point)

        count_numscan += 1

        catalog_editor['finished'][i] = 1
        catalog_editor.to_csv(catalog_csv, index=False)
        